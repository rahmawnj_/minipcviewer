<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['student_model', 'Teacher_staff_model', 'teacher_staff_attandance_model', 'holiday_model', 'student_attandance_model', 'attandance_device_model', 'operational_time_model', 'setting_model']);
        $this->load->helper(['form', 'url']);
    }

    public function send_student_wa($siswa, $waktu, $tanggal, $day, $ket)
    {
        $message_data = $this->setting_model->get_setting('name', 'whatsapp_message_student');
        // convert message
        $pesans = explode(' ', $message_data['value']);
        $message = '';
        foreach ($pesans as $pesan) {
            if (strpos($pesan, '$') !== false) {
                if ($pesan == '$nama') {
                    $message .= ' ' . $siswa['nama'];
                } else if ($pesan == '$kelas') {
                    $message .= ' ' . $siswa['kelas'];
                } else if ($pesan == '$waktu') {
                    $message .= ' ' . $waktu;
                } else if ($pesan == '$keterangan') {
                    $message .= ' ' . $ket;
                } else if ($pesan == '$tanggal') {
                    $message .= ' ' . $tanggal;
                } else if ($pesan == '$hari') {
                    $message .= ' ' . $day;
                } else {
                    $message .= ' ' . $pesan;
                }
            } else {
                $message .= ' ' . $pesan;
            }
        }

        $message = trim($message);
        if ($siswa['no_hp_ortu'] && (strlen($siswa['no_hp_ortu']) > 0 && $siswa['no_hp_ortu'][0] !== "0")) {
            // $data = array(
            //     "api_key" => $this->setting_model->get_setting('name', 'api_key_wa')['value'],
            //     "sender" => $this->setting_model->get_setting('name', 'sender_number')['value'],
            //     "number" => '62' . $siswa['no_hp_ortu'],
            //     "message" => $message
            // );

            // $jsonData = json_encode($data);

            // $opts = array(
            //     'http' => array(
            //         'method' => 'POST',
            //         'header' => "Content-Type: application/json\r\n" .
            //             "Content-Length: " . strlen($jsonData) . "\r\n",
            //         'content' => $jsonData
            //     )
            // );

            // $context = stream_context_create($opts);
            // $response = file_get_contents('https://chat.smpn23depok.sch.id/send-message', false, $context);

            if ($this->setting_model->get_setting('name', 'whatsapp_message_student')['value'] != null) {
                $data = array(
                    "api_key" => $this->setting_model->get_setting('name', 'api_key_wa')['value'],
                    "sender" => $this->setting_model->get_setting('name', 'sender_number')['value'],
                    "number" => '62' . $siswa['no_hp_ortu'],
                    "message" => $message
                );

                $options = array(
                    'http' => array(
                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method'  => 'POST',
                        'content' => http_build_query($data)
                    )
                );
                $url = $this->setting_model->get_setting('name', 'wabot')['value'];
                $context  = stream_context_create($options);
                // $response = file_get_contents($url, false, $context);
                $response = file_get_contents($url, false, $context);

                return $response;
            }
        }
    }

    public function send_teacherstaff_wa($teacher_staff, $waktu, $tanggal, $day, $ket)
    {
        $message_data = $this->setting_model->get_setting('name', 'whatsapp_message_teacherstaff');
        // convert message
        $pesans = explode(' ', $message_data['value']);
        $message = '';
        foreach ($pesans as $pesan) {
            if (strpos($pesan, '$') !== false) {
                if ($pesan == '$nama') {
                    $message .= ' ' . $teacher_staff['nama'];
                } else if ($pesan == '$jabatan') {
                    $message .= ' ' . $teacher_staff['jabatan'];
                } else if ($pesan == '$waktu') {
                    $message .= ' ' . $waktu;
                } else if ($pesan == '$keterangan') {
                    $message .= ' ' . $ket;
                } else if ($pesan == '$tanggal') {
                    $message .= ' ' . $tanggal;
                } else if ($pesan == '$hari') {
                    $message .= ' ' . $day;
                } else {
                    $message .= ' ' . $pesan;
                }
            } else {
                $message .= ' ' . $pesan;
            }
        }
        $message = trim($message);
        if ($this->setting_model->get_setting('name', 'whatsapp_message_teacherstaff')['value'] != null) {
            if (strlen($teacher_staff['no_hp']) > 0 && $teacher_staff['no_hp'][0] !== "0") {
                file_get_contents($this->setting_model->get_setting('name', 'wabot')['value'], false, stream_context_create([
                    'http' => array(
                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method'  => 'POST',
                        'content' => http_build_query([
                            "api_key" => $this->setting_model->get_setting('name', 'api_key_wa')['value'],
                            "sender" => $this->setting_model->get_setting('name', 'sender_number')['value'],
                            "number" => '62' . $teacher_staff['no_hp'],
                            "message" => $message
                        ])
                    )
                ]));


                if ($this->setting_model->get_setting('name', 'no_pimpinan1')['value'] != null) {
                    file_get_contents($this->setting_model->get_setting('name', 'wabot')['value'], false, stream_context_create([
                        'http' => array(
                            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                            'method'  => 'POST',
                            'content' => http_build_query([
                                "api_key" => $this->setting_model->get_setting('name', 'api_key_wa')['value'],
                                "sender" => $this->setting_model->get_setting('name', 'sender_number')['value'],
                                "number" => $this->setting_model->get_setting('name', 'no_pimpinan1')['value'],
                                "message" => $message
                            ])
                        )
                    ]));
                }

                if ($this->setting_model->get_setting('name', 'no_pimpinan2')['value'] != null) {
                    file_get_contents($this->setting_model->get_setting('name', 'wabot')['value'], false, stream_context_create([
                        'http' => array(
                            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                            'method'  => 'POST',
                            'content' => http_build_query([
                                "api_key" => $this->setting_model->get_setting('name', 'api_key_wa')['value'],
                                "sender" => $this->setting_model->get_setting('name', 'sender_number')['value'],
                                "number" => $this->setting_model->get_setting('name', 'no_pimpinan2')['value'],
                                "message" => $message
                            ])
                        )
                    ]));
                }
            }
        }
    }

    public function last_log_get($id = null)
    {
        $device = $this->db->get_where('attandance_devices', ['device' => $id])->row_array();
        if ($device) {
            $rfid = $device['rfid'];

            $student = $this->db->join('classes', 'classes.id_class = students.id_class')->where(['students.deleted' => 0])->get_where('students', ['rfid' => $rfid])->row_array();
            $teacher_staff = $this->db->where(['teacher_staffs.deleted' => 0])->get_where('teacher_staffs', ['rfid' => $rfid])->row_array();
            $type = null;
            if ($student) {
                $type = 'student';
                $data = $student;
            } else if ($teacher_staff) {
                $type = 'teacher_staff';
                $data = $teacher_staff;
            }

            $this->response(['msg' => 'success', 'type' => $type, 'data' => $data, 'device' => $device]);
        } else {
            $this->response(['msg' => 'success', 'logs' => 'id-tidak-terdaftar', 'data_user' => '']);
        }
    }


    public function absensijson_post()
    {

        if (is_null($this->input->post('key'))) {
            $this->response(['ket' => 'key perlu dikirim', 'status' => 'error']);
        }
        if (is_null($this->input->post('id_device'))) {
            $this->response(['ket' => 'id_device perlu dikirim', 'status' => 'error']);
        }
        if (is_null($this->input->post('rfid'))) {
            $this->response(['ket' => 'rfid perlu dikirim', 'status' => 'error']);
        }

        $setting_secret_key = $this->setting_model->get_setting('name', 'secret_key');
        if ($setting_secret_key['value'] == $this->input->post('key')) {

            $device = $this->attandance_device_model->get_attandance_device('device', $this->input->post('id_device'));
            if ($device) {
                // $student = $this->student_model->get_student('rfid', $this->input->post('rfid'));
                $student =     $this->db->join('classes', 'classes.id_class = students.id_class')->join('operational_times', 'operational_times.id_operational_time = classes.id_operational_time')->where(['students.deleted' => 0, 'rfid' =>  $this->input->post('rfid')])->get('students')->row_array();
                $teacher_staff = $this->Teacher_staff_model->get_teacher_staff('rfid', $this->input->post('rfid'));

                $type = null;
                if ($student) {
                    $type = 'student';
                    $typeUser = 'siswa';

                    $masuk = explode('-', $student['waktu_masuk']);
                    $keluar = explode('-', $student['waktu_keluar']);
                    $masuk_awal = $masuk[0];
                    $masuk_akhir = $masuk[1];
                    $keluar_awal = $keluar[0];
                    $keluar_akhir = $keluar[1];
                    $telat = $student['telat'];
                } else if ($teacher_staff) {
                    $type = 'teacher_staff';
                    $typeUser = 'guru';

                    $masuk = explode('-', $teacher_staff['waktu_masuk']);
                    $keluar = explode('-', $teacher_staff['waktu_keluar']);
                    $masuk_awal = $masuk[0];
                    $masuk_akhir = $masuk[1];
                    $keluar_awal = $keluar[0];
                    $keluar_akhir = $keluar[1];
                    $telat = $teacher_staff['telat'];
                }
                $this->db->update('attandance_devices', [
                    'rfid' => $this->input->post('rfid'),
                    'time' => date('Y-m-d H:i:s')
                ], ['id_attandance_device' => $this->input->post('id_device')]);

                if ($type) {
                    $date_now = date('Y-m-d');
                    $day_now = date('l');
                    $time_now = date("H:i");

                    $holiday = $this->db->get_where('holidays', ['waktu' => $date_now, 'type' => $type])->row_array();
                    $weekly_holiday = $this->db->get('weekly_holidays')->result_array();
                    $weekend_attendance = $this->db->get_where('settings', array('name' => 'weekend_attendance'))->row_array();

                    if (!$holiday && !in_array($day_now, array_column($weekly_holiday, 'hari'))) {

                        if ($time_now < $masuk_akhir && $time_now > $masuk_awal) {
                            if ($type == 'student') {
                                $user = $student;
                                $attandance = $this->student_attandance_model->get_student_attandace($date_now, $student['id_student']);
                            } else {
                                $user = $teacher_staff;
                                $attandance = $this->teacher_staff_attandance_model->get_teacher_staff_attandace($date_now, $teacher_staff['id_teacher_staff']);
                            }
                            if (!$attandance) {
                                if ($time_now < $telat) {
                                    $ket = 'Hadir - Tepat waktu';
                                } else {
                                    $ket = 'Hadir - Telat';
                                }
                                if ($type == 'student') {
                                    $this->db->insert('student_attandances', [
                                        'id_device' => $device[0]['id_attandance_device'],
                                        'id_student' => $student['id_student'],
                                        'masuk' => 1,
                                        'waktu_masuk' => $time_now,
                                        'keluar' => 0,
                                        'status_hadir' => 'Hadir',
                                        'ket'  => $ket,
                                        'date' => $date_now,
                                    ]);

                                    $this->send_student_wa($student, $time_now, $date_now, $day_now, $ket);
                                } else {
                                    $this->teacher_staff_attandance_model->insert([
                                        'id_device' => $device[0]['id_attandance_device'],
                                        'id_teacher_staff' => $teacher_staff['id_teacher_staff'],
                                        'masuk' => 1,
                                        'waktu_masuk' => $time_now,
                                        'keluar' => 0,
                                        'status_hadir' => 'Hadir',
                                        'ket'  => $ket,
                                        'date' => $date_now,
                                    ]);
                                    $this->send_teacherstaff_wa($teacher_staff, $time_now, $date_now, $day_now, $ket);
                                }
                                $this->response(['ket' => $ket, 'status' => 'success', 'type' => $typeUser, $typeUser => $user, 'waktu' => date('H:i:s d/m/Y')]);
                            } else {
                                $this->response(['ket' => 'Sudah Absen', 'status' => 'success', 'type' => $typeUser, $typeUser => $user, 'waktu' => '-']);
                            }
                        } else if ($time_now > $keluar_awal && $time_now < $keluar_akhir) {
                            if ($type == 'student') {
                                $user = $student;
                                $attandance = $this->student_attandance_model->get_student_attandace($date_now, $student['id_student']);
                            } else {
                                $user = $teacher_staff;
                                $attandance = $this->teacher_staff_attandance_model->get_teacher_staff_attandace($date_now, $teacher_staff['id_teacher_staff']);
                            }
                            if ($attandance) {
                                $ket = 'pulang';
                                if ($type == 'student') {
                                    $this->student_attandance_model->update($attandance['id_student_attandance'], [
                                        'keluar' => 1,
                                        'waktu_keluar' => $time_now,
                                    ]);
                                    $this->send_student_wa($student, $time_now, $date_now, $day_now, $ket);
                                } else {
                                    $this->teacher_staff_attandance_model->update($attandance['id_teacher_staff_attandance'], [
                                        'keluar' => 1,
                                        'waktu_keluar' => $time_now,
                                    ]);
                                    $this->send_teacherstaff_wa($teacher_staff, $time_now, $date_now, $day_now, $ket);
                                }
                                $this->response(['ket' => $ket, 'status' => 'success', 'type' => $typeUser, $typeUser => $user, 'waktu' => date('H:i:s d/m/Y')]);
                            } else {
                                $this->response(['ket' => 'Absensi Gagal', 'status' => 'error']);
                            }
                        } else {
                            $this->response(['ket' => 'Error waktu operasional', 'status' => 'error']);
                        }
                    } else {
                        if ($weekend_attendance['value'] == 'on') {

                            if ($time_now < $masuk_akhir && $time_now > $masuk_awal) {
                                if ($type == 'student') {
                                    $user = $student;
                                    $attandance = $this->student_attandance_model->get_student_attandace($date_now, $student['id_student']);
                                } else {
                                    $user = $teacher_staff;
                                    $attandance = $this->teacher_staff_attandance_model->get_teacher_staff_attandace($date_now, $teacher_staff['id_teacher_staff']);
                                }
                                if (!$attandance) {
                                    if ($time_now < $telat) {
                                        $ket = 'Hadir - Tepat waktu';
                                    } else {
                                        $ket = 'Hadir - Telat';
                                    }
                                    if ($type == 'student') {
                                        $this->db->insert('student_attandances', [
                                            'id_device' => $device[0]['id_attandance_device'],
                                            'id_student' => $student['id_student'],
                                            'masuk' => 1,
                                            'waktu_masuk' => $time_now,
                                            'keluar' => 0,
                                            'status_hadir' => 'Hadir',
                                            'ket'  => $ket,
                                            'date' => $date_now,
                                        ]);
                                        $this->send_student_wa($student, $time_now, $date_now, $day_now, $ket);
                                    } else {
                                        $this->teacher_staff_attandance_model->insert([
                                            'id_device' => $device[0]['id_attandance_device'],
                                            'id_teacher_staff' => $teacher_staff['id_teacher_staff'],
                                            'masuk' => 1,
                                            'waktu_masuk' => $time_now,
                                            'keluar' => 0,
                                            'status_hadir' => 'Hadir',
                                            'ket'  => $ket,
                                            'date' => $date_now,
                                        ]);
                                        $this->send_teacherstaff_wa($teacher_staff, $time_now, $date_now, $day_now, $ket);
                                    }
                                    $this->response(['ket' => $ket, 'status' => 'success', 'type' => $typeUser, $typeUser => $user, 'waktu' => date('H:i:s d/m/Y')]);
                                } else {
                                    $this->response(['ket' => 'Sudah Absen', 'status' => 'error']);
                                }
                            } else if ($time_now > $keluar_awal && $time_now < $keluar_akhir) {
                                if ($type == 'student') {
                                    $user = $student;
                                    $attandance = $this->student_attandance_model->get_student_attandace($date_now, $student['id_student']);
                                } else {
                                    $user = $teacher_staff;
                                    $attandance = $this->teacher_staff_attandance_model->get_teacher_staff_attandace($date_now, $teacher_staff['id_teacher_staff']);
                                }
                                if ($attandance) {
                                    $ket = 'pulang';
                                    if ($type == 'student') {
                                        $this->student_attandance_model->update($attandance['id_student_attandance'], [
                                            'keluar' => 1,
                                            'waktu_keluar' => $time_now,
                                        ]);
                                        $this->send_student_wa($student, $time_now, $date_now, $day_now, $ket);
                                    } else {
                                        $this->teacher_staff_attandance_model->update($attandance['id_teacher_staff_attandance'], [
                                            'keluar' => 1,
                                            'waktu_keluar' => $time_now,
                                        ]);
                                        $this->send_teacherstaff_wa($teacher_staff, $time_now, $date_now, $day_now, $ket);
                                    }
                                    $this->response(['ket' => $ket, 'status' => 'success', 'type' => $typeUser, $typeUser => $user, 'waktu' => date('H:i:s d/m/Y')]);
                                } else {
                                    $this->response(['ket' => 'Absensi Gagal', 'status' => 'error']);
                                }
                            } else {
                                $this->response(['ket' => 'Error waktu operasional', 'status' => 'error']);
                            }
                        } else {
                            $this->response(['ket' => 'Hari Libur', 'status' => 'error']);
                        }
                    }
                } else {
                    $this->response(['ket' => 'rfid tidak ditemukan', 'status' => 'error']);
                }
            } else {
                $this->response(['ket' => 'id_device tidak ditemukan', 'status' => 'error']);
            }
        } else {
            $this->response(['ket' => 'key salah', 'status' => 'error']);
        }
    }


    public function gettime_get()
    {
        $now = new DateTime();
        $time = $now->format('Y, m, d, H, i, s');

        $this->response(['status' => 'success', 'time' => $time]);
    }

}
