@extends('layouts.app')

@push('styles')
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('plugins/summernote-0.8.18-dist/popper.min.js') }}" crossorigin="anonymous">
    </script>

    <link rel="stylesheet" href="{{ asset('plugins/summernote-0.8.18-dist/bootstrap.min.css') }}" crossorigin="anonymous">
    <script src="{{ asset('plugins/summernote-0.8.18-dist/bootstrap.min.js') }}" crossorigin="anonymous"></script>

    <link href="{{ asset('plugins/summernote-0.8.18-dist/summernote-bs4.min.css') }}" rel="stylesheet">
    <script src="{{ asset('plugins/summernote-0.8.18-dist/summernote-bs4.min.js') }}"></script>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Settings') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('settings.update') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <label for="id_device" class="form-label">ID Device</label>
                                <input type="text" id="id_device" name="id_device"
                                    value="{{ old('id_device', App\Models\Setting::where('name', 'id_device')->first()->description) }}"
                                    class="form-control">
                                @error('id_device')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="mask_text" class="form-label">Mask Text</label>
                                <textarea id="mask_text" name="mask_text" class="form-control summernote">{{ old('mask_text', App\Models\Setting::where('name', 'mask_text')->first()->description) }}</textarea>
                                @error('mask_text')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="stranger_text" class="form-label">Stranger Text</label>
                                <textarea id="stranger_text" name="stranger_text" class="form-control summernote">{{ old('stranger_text', App\Models\Setting::where('name', 'stranger_text')->first()->description) }}</textarea>
                                @error('stranger_text')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="confirm_text" class="form-label">Confirm Text</label>
                                <textarea id="confirm_text" name="confirm_text" class="form-control summernote">{{ old('confirm_text', App\Models\Setting::where('name', 'confirm_text')->first()->description) }}</textarea>
                                @error('confirm_text')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="info" class="form-label">Info</label>
                                <textarea id="info" name="info" class="form-control summernote">{{ old('info', App\Models\Setting::where('name', 'info')->first()->description) }}</textarea>
                                @error('info')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="error_sound" class="form-label">Failed Sound</label>
                                <input type="file" id="error_sound" name="error_sound" class="form-control">
                                @error('error_sound')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <audio controls>
                                    <source
                                        src="{{ Storage::url(App\Models\Setting::where('name', 'error_sound')->first()->description) }}"
                                        type="audio/mpeg">
                                </audio>
                            </div>

                            <div class="mb-3">
                                <label for="confirm_sound" class="form-label">Confirm Sound</label>
                                <input type="file" id="confirm_sound" name="confirm_sound" class="form-control">
                                @error('confirm_sound')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                {{-- Kolom Putar MP3 --}}
                                <audio controls>
                                    <source
                                        src="{{ Storage::url(App\Models\Setting::where('name', 'confirm_sound')->first()->description) }}"
                                        type="audio/mpeg">
                                </audio>
                            </div>

                            <div class="mb-3">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.summernote').summernote();
        });
    </script>
@endpush
