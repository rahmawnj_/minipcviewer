<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-5.3.0/dist/css/bootstrap.min.css') }}">

    <link rel="icon" type="image/png" href="{{ asset('fav.ico') }}">

    <title>PT KALBE MORINAGA</title>
    <style>
        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        .logo {
            position: absolute;
            top: 10px;
            left: 10px;
            height: 50px;
        }

        .status-indicator {
            width: 10px;
            height: 10px;
            border-radius: 50%;
            display: inline-block;
            margin-left: 5px;
        }

        .online {
            background-color: green;
        }

        .offline {
            background-color: red;
        }

        .middle-box {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 200px;
            margin-top: 20px;
        }
    </style>
</head>

<body style="background-color: bg-white;">
    <div class="container">
        <img src="{{ asset('Logo Kalbe.png') }}" alt="Logo" class="logo">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="card" style="background-color: #7cc242; height: 450px;">
                    <div class="card-header ">
                        <div class="text-end">
                            <span id="indicator" class="status-indicator"></span>
                            <b id="device-status">DEVICE OFFLINE</b>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="row mt-5">
                            <div class="col-md-3">
                                <img id="preview-image" style="height: 200px; display:none;" alt="Foto"
                                    class="rounded">
                            </div>
                            <div class="col-md-9">
                                <h2 id="nama" class="mb-3"></h2>
                                <h5 id="nik" class="mb-3"></h5>
                                <h5 id="jabatan" class="mb-3"></h5>
                                <h5 id="temperature" class="mb-3"></h5>
                            </div>
                        </div>
                        <h3 id="welcome" class="text-white text-center middle-box">
                            HEALTH DECLARE PT KALBE MORINAGA INDONESIA
                        </h3>
                    </div>

                    <div class="card-footer">
                        <div class="card text-white bg-dark" id="messagebox" style="display: none">
                            <div class="card-body text-center ">
                                <h5 class="card-title"></h5>
                                <audio id="error_sound" hidden controls autoplay muted>
                                    <source
                                        src="{{ Storage::url(App\Models\Setting::where('name', 'error_sound')->first()->description) ?? asset('music/classic-alarm.wav') }}"
                                        type="audio/mp3">
                                </audio>
                                <audio id="confirm_sound" hidden controls autoplay muted>
                                    <source
                                        src="{{ Storage::url(App\Models\Setting::where('name', 'confirm_sound')->first()->description) ?? asset('music/suspense-waiting.wav') }}"
                                        type="audio/mp3">
                                </audio>
                                <p id="message" class="card-text"></p>
                                <div id="confirmation-form" style="display: none;">
                                    <button id="yes-button" class="btn btn-primary">yes</button>
                                    <button id="no-button" class="btn btn-danger">no</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var error_sound = document.getElementById("error_sound");
        var confirm_sound = document.getElementById("confirm_sound");
        var stopPreviewUpdate = false; // Variabel penanda untuk menghentikan pembaruan preview

        function updatePreview() {
            if (stopPreviewUpdate) {
                return; // Menghentikan pembaruan preview jika variabel stopPreviewUpdate true
            }

            var imageElement = document.getElementById('preview-image');

            fetch('api/lastlog')
                .then(response => response.json())
                .then(data => {

                    console.log(data);
                    var currentTime = new Date().getTime();
                    var imageTime = new Date(data.time).getTime();
                    var heartbeatTime = new Date(data.heartbeat).getTime();

                    if (currentTime - heartbeatTime > 10000) {
                        document.getElementById("device-status").innerHTML = "DEVICE OFFLINE";
                        document.getElementById("indicator").style.backgroundColor = 'red';
                    } else {
                        document.getElementById("device-status").innerHTML = "DEVICE ONLINE";
                        document.getElementById("indicator").style.backgroundColor = 'green';
                    }
                    if (currentTime - imageTime > 10000 && ((data.operator === 'VerifyPush' && data.is_export === 1) ||
                            (data.operator === 'VerifyPush' && data.mask === 0) ||
                            data.operator == 'SnapPush')) {
                        imageElement.style.display = 'none';
                        document.getElementById('welcome').style.display = 'block';
                        // document.getElementById('welcome').classList.add('middle-box');
                        document.getElementById('message').textContent = '';
                        document.getElementById('nama').textContent = '';
                        document.getElementById('temperature').textContent = '';
                        error_sound.pause();
                        error_sound.muted = true;
                        confirm_sound.pause();
                        confirm_sound.muted = true;

                        document.getElementById('messagebox').style.display = 'none';


                    } else {
                        document.getElementById('welcome').style.display =
                            'none';

                        imageElement.style.display = 'block';
                        imageElement.src = data.picture;
                        document.getElementById('temperature').textContent = 'SUHU :' + data.temperature + '°C';
                        if (data.operator === 'VerifyPush') {
                            document.getElementById('nama').textContent = 'NAMA : ' + data.name;
                            document.getElementById('messagebox').style.display = 'block';
                            setTimeout(() => {
                                if (!data.mask) {
                                    error_sound.muted = false;
                                    error_sound.play();
                                    console.log(data.name);
                                    document.getElementById('message').textContent =
                                        `{{ App\Models\Setting::where('name', 'mask_text')->first()->description }}`;
                                } else if (data.is_export === 0 && data.mask) {
                                    document.getElementById('message').textContent =
                                        "{{ App\Models\Setting::where('name', 'confirm_text')->first()->description }}";
                                    error_sound.pause();
                                    error_sound.muted = true;
                                    stopPreviewUpdate = true; // Menghentikan pembaruan preview

                                    showConfirmationPrompt(data);
                                }
                            }, 1000); // Menunda tampilan prompt selama 1 detik setelah gambar ditampilkan
                        } else {
                            document.getElementById('messagebox').style.display =
                                'none';
                            document.getElementById('nama').textContent = 'WAJAH TIDAK TERDAFTAR';
                        }
                    }
                })
                .catch(error => {
                    console.error('Terjadi kesalahan:', error);
                })
                .finally(() => {
                    if (!stopPreviewUpdate) {
                        setTimeout(updatePreview,
                            1000); // Mengulangi pembaruan setiap detik jika stopPreviewUpdate false
                    }
                });
        }

        function showConfirmationPrompt(data) {
            console.log('please confirm');
            document.getElementById('confirmation-form').style.display = 'block'; // Menampilkan form konfirmasi
            confirm_sound.muted = false;
            confirm_sound.play();
            var yesButton = document.getElementById('yes-button');
            var noButton = document.getElementById('no-button');

            yesButton.addEventListener('click', function() {
                console.log('yes confirm');
                handleConfirmation(data, true); // Panggil fungsi handleConfirmation dengan respons 'Yes'
            });

            noButton.addEventListener('click', function() {
                console.log('no confirm');
                handleConfirmation(data, false); // Panggil fungsi handleConfirmation dengan respons 'No'
            });
        }

        function handleConfirmation(data, userResponse) {
            document.getElementById('confirmation-form').style.display =
                'none';
            document.getElementById('messagebox').style.display =
                'none';
            document.getElementById('message').style.display =
                'none';

            sendAPI(data, userResponse); // Panggil fungsi sendAPI dengan respons pengguna
        }

        function sendAPI(data, userResponse) {
            var payload = {
                data: data,
                response: userResponse ? 'Yes' :
                    'No' // Mengirim respons 'Yes' jika userResponse true, 'No' jika userResponse false
            };
            fetch('api/export', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(payload)
                })
                .then(response => response.json())
                .then(responseData => {
                    console.log(responseData);
                    // Tambahkan logika atau tindakan setelah API terkirim berhasil
                    stopPreviewUpdate =
                        false; // Set stopPreviewUpdate kembali ke false untuk melanjutkan pembaruan preview
                    updatePreview();
                    location.reload()
                })
                .catch(error => {
                    console.log(error);
                    stopPreviewUpdate =
                        false; // Set stopPreviewUpdate kembali ke false untuk melanjutkan pembaruan preview
                    updatePreview();
                });
        }

        // Memulai pembaruan previhttps://chat.openai.com/c/f08b5fec-d704-4352-bc3b-5b182db5ae16ew saat halaman dimuat
        document.addEventListener('DOMContentLoaded', () => {
            updatePreview();
        });
    </script>

</body>

</html>
