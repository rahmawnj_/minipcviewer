<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_images', function (Blueprint $table) {
            $table->id();
            $table->string('operator')->nullable();
            $table->string('uuid')->nullable();
            $table->string('name')->nullable();
            $table->longText('picture')->nullable();
            $table->boolean('mask')->default(0);
            $table->string('temperature')->default(0);
            $table->timestamp('heartbeat')->nullable();
            $table->timestamp('time');
            $table->boolean('is_export')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_images');
    }
}
