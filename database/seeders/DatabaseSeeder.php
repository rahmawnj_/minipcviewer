<?php

namespace Database\Seeders;

use App\Models\LogImage;
use App\Models\User;
use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'password' => Hash::make('admin'),
        ]);
        Setting::create([
            'name' => 'id_device',
            'description' => '1935378'
        ]);

        Setting::create([
            'name' => 'info',
            'description' => '<h4>Halo $nama, Absensimu berhasil disimpan dengan suhu $suhu&nbsp;</h4><h4><b style="font-size: 1.5rem; color: var(--bs-card-color);">Selamat Bekerja</b></h4>'
        ]);

        Setting::create([
            'name' => 'error_sound',
            'description' => ''
        ]);

        Setting::create([
            'name' => 'confirm_sound',
            'description' => ''
        ]);

        Setting::create([
            'name' => 'confirm_text',
            'description' => '<h4><b>Apakah anda berkomitmen</b> untuk menerapkan <b>GMSP</b> di lingkungan KMI</h4><p><br></p>'
        ]);
        Setting::create([
            'name' => 'stranger_text',
            'description' => '<h4 style="text-align: left;"><br></h4><h4 style="text-align: left;">Tidak Dikenali</h4>'
        ]);

        Setting::create([
            'name' => 'mask_text',
            'description' => '<h4><b><br></b></h4><h4><b>Harap Lepaskan Masker</b></h4>'
        ]);

        Setting::create([
            'name' => 'show_image_duration',
            'description' => '5'
        ]);
        Setting::create([
            'name' => 'status_log',
            'description' => true
        ]);

        LogImage::create([
            'operator' => 'Operator 1',
            'uuid' => 29,
            'name' => 'Image 1',
            'picture' => '',
            'temperature' => '30.5',
            'time' => now(),
            'is_export' => false,
        ]);

        // Setting::create([
        //     'name' => 'iddevice_facerecogition',
        //     'description' => '98982434'
        // ]);
        // Setting::create([
        //     'name' => 'ipaddress_facerecogition',
        //     'description' => '192.99.168.21'
        // ]);
    }
}
