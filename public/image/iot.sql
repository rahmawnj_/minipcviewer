-- -------------------------------------------------------------
-- TablePlus 5.3.8(500)
--
-- https://tableplus.com/
--
-- Database: iot
-- Generation Time: 2023-08-14 09:38:42.1920
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."alarm_histories";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS alarm_histories_id_seq;

-- Table Definition
CREATE TABLE "public"."alarm_histories" (
    "id" int4 NOT NULL DEFAULT nextval('alarm_histories_id_seq'::regclass),
    "tag_name" varchar(200) NOT NULL,
    "value" int4 NOT NULL,
    "created_at" timestamptz(0) NOT NULL,
    "updated_at" timestamptz(0) NOT NULL,
    "machine_id" int4,
    "operator" varchar(10),
    "standard" int4,
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."alarms";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS alarms_id_seq;

-- Table Definition
CREATE TABLE "public"."alarms" (
    "id" int4 NOT NULL DEFAULT nextval('alarms_id_seq'::regclass),
    "tag_name_id" int4 NOT NULL,
    "name" varchar(100) NOT NULL,
    "operator" varchar(10),
    "value" int4 NOT NULL,
    "timer" int4 NOT NULL,
    "created_at" timestamptz(0) NOT NULL,
    "updated_at" timestamptz(0) NOT NULL,
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."areas";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS areas_id_seq;

-- Table Definition
CREATE TABLE "public"."areas" (
    "id" int4 NOT NULL DEFAULT nextval('areas_id_seq'::regclass),
    "code" varchar(10) NOT NULL,
    "name" varchar(50) NOT NULL,
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."form_parameter";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS form_parameter_id_seq;

-- Table Definition
CREATE TABLE "public"."form_parameter" (
    "id" int4 NOT NULL DEFAULT nextval('form_parameter_id_seq'::regclass),
    "name" varchar(150),
    "no_document" varchar(100),
    "created_by" varchar(50),
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."form_parameter_list";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS form_parameter_id_seq;

-- Table Definition
CREATE TABLE "public"."form_parameter_list" (
    "id" int4 NOT NULL DEFAULT nextval('form_parameter_id_seq'::regclass),
    "parameter" varchar(50),
    "type" varchar(20),
    "tag_name" varchar(100),
    "form_parameter_id" int4,
    "created_by" varchar(50),
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."form_parameter_list_value";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS form_parameter_id_seq;

-- Table Definition
CREATE TABLE "public"."form_parameter_list_value" (
    "id" int4 NOT NULL DEFAULT nextval('form_parameter_id_seq'::regclass),
    "form_parameter_value_id" int4,
    "value" varchar(150),
    "created_by" varchar(50),
    "updated_by" varchar(50),
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    "time" varchar(20),
    "parameter" varchar(50),
    "parameter_id" int4,
    "shift" int4,
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."form_parameter_value";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS form_parameter_id_seq;

-- Table Definition
CREATE TABLE "public"."form_parameter_value" (
    "id" int4 NOT NULL DEFAULT nextval('form_parameter_id_seq'::regclass),
    "okp" varchar(20),
    "no_document" varchar(100),
    "kode_produksi" varchar(100),
    "size" varchar(20),
    "line" varchar(20),
    "shift" varchar(10),
    "created_by" varchar(50),
    "expire_date" timestamptz(0),
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    "nama_product" varchar(50),
    "form_parameter_id" int4,
    "stop" bool,
    "form_name" varchar(50),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."line_processes";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS line_processes_id_seq;

-- Table Definition
CREATE TABLE "public"."line_processes" (
    "id" int4 NOT NULL DEFAULT nextval('line_processes_id_seq'::regclass),
    "code" varchar(20) NOT NULL,
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    "name" varchar(20),
    "edges" json,
    "style" varchar(500),
    "background" varchar(200),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."logs";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS logs_id_seq;

-- Table Definition
CREATE TABLE "public"."logs" (
    "id" int4 NOT NULL DEFAULT nextval('logs_id_seq'::regclass),
    "topic" varchar(100),
    "message" varchar(200),
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."machines";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS machines_id_seq;

-- Table Definition
CREATE TABLE "public"."machines" (
    "id" int4 NOT NULL DEFAULT nextval('machines_id_seq'::regclass),
    "name" varchar(100) NOT NULL,
    "line_process_id" int4,
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    "area_id" int4,
    "port_in" int4,
    "port_out" int4,
    "size" varchar(10),
    "position" json,
    "style" json,
    "image_offline" varchar(200),
    "image_run" varchar(200),
    "image_stop" varchar(200),
    "status" varchar(20),
    "timeout" int4,
    "last_call" timestamptz(0),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."notifications";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."notifications" (
    "id" int4 NOT NULL,
    "name" varchar(100) NOT NULL,
    "created_at" timestamptz(0) NOT NULL,
    "updated_at" timestamptz(0) NOT NULL,
    "username" varchar(200) NOT NULL,
    "password" varchar(200),
    "is_active" bool NOT NULL,
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."shift";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS shift_id_seq;

-- Table Definition
CREATE TABLE "public"."shift" (
    "id" int4 NOT NULL DEFAULT nextval('shift_id_seq'::regclass),
    "name" varchar(50) NOT NULL,
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."shifts";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS shifts_id_seq;

-- Table Definition
CREATE TABLE "public"."shifts" (
    "id" int4 NOT NULL DEFAULT nextval('shifts_id_seq'::regclass),
    "name" varchar,
    "time_start" varchar,
    "time_end" varchar,
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."tag_names";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS tag_names_id_seq;

-- Table Definition
CREATE TABLE "public"."tag_names" (
    "id" int4 NOT NULL DEFAULT nextval('tag_names_id_seq'::regclass),
    "name" varchar(20),
    "visibility" varchar(10),
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    "type" varchar(10),
    "description" text,
    PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "public"."tag_names_machines";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."tag_names_machines" (
    "tag_name_id" int4,
    "machine_id" int4
);

DROP TABLE IF EXISTS "public"."users";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS users_id_seq;

-- Table Definition
CREATE TABLE "public"."users" (
    "id" int4 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
    "name" varchar(50) NOT NULL,
    "phone" varchar(20) NOT NULL,
    "email" varchar(50) NOT NULL,
    "password" varchar(200) NOT NULL,
    "photo" varchar(200),
    "created_at" timestamptz(0) NOT NULL,
    "updated_at" timestamptz(0) NOT NULL,
    "type" varchar(20),
    "shift_id" int4,
    PRIMARY KEY ("id")
);

INSERT INTO "public"."areas" ("id", "code", "name", "created_at", "updated_at") VALUES
(6, 'A1', 'Area 1', NULL, NULL),
(7, 'A2', 'Area 2', NULL, NULL);

INSERT INTO "public"."form_parameter" ("id", "name", "no_document", "created_by", "created_at", "updated_at") VALUES
(45, 'Form Satu', 'FRM', 'Admin', '2023-06-07 11:03:43+07', '2023-06-07 11:03:43+07');

INSERT INTO "public"."form_parameter_list" ("id", "parameter", "type", "tag_name", "form_parameter_id", "created_by", "created_at", "updated_at") VALUES
(50, 'Param 1', 'manual', '-', 45, NULL, '2023-06-08 18:26:37+07', '2023-06-08 18:26:37+07'),
(51, 'Param 2', 'manual', '-', 45, NULL, '2023-06-08 18:26:37+07', '2023-06-08 18:26:37+07'),
(52, 'Param 3', 'manual', '-', 45, NULL, '2023-06-08 18:26:37+07', '2023-06-08 18:26:37+07'),
(53, 'Param 4', 'mesin', '/testing/asd', 45, NULL, '2023-06-08 18:26:37+07', '2023-06-08 18:26:37+07');

INSERT INTO "public"."form_parameter_list_value" ("id", "form_parameter_value_id", "value", "created_by", "updated_by", "created_at", "updated_at", "time", "parameter", "parameter_id", "shift") VALUES
(55, 54, '1', 'Admin', NULL, '2023-06-08 18:27:47+07', '2023-06-08 18:27:47+07', '18:30', 'Param 2', 51, NULL),
(56, 54, '0', 'Admin', NULL, '2023-06-08 18:27:47+07', '2023-06-08 18:27:47+07', '18:30', 'Param 4', 53, NULL),
(57, 54, '2', 'Admin', NULL, '2023-06-08 19:02:45+07', '2023-06-08 19:02:45+07', '19:00', 'Param 1', 50, NULL),
(58, 54, '0', 'Admin', NULL, '2023-06-08 19:02:45+07', '2023-06-08 19:02:45+07', '19:00', 'Param 4', 53, NULL);

INSERT INTO "public"."form_parameter_value" ("id", "okp", "no_document", "kode_produksi", "size", "line", "shift", "created_by", "expire_date", "created_at", "updated_at", "nama_product", "form_parameter_id", "stop", "form_name") VALUES
(54, 'OKP', NULL, 'xxxx', '1', 'Line 1', NULL, 'Admin', '2023-06-30 00:00:00+07', '2023-06-08 18:27:47+07', '2023-06-08 18:27:47+07', 'Produk', 45, 'f', 'Form Satu');

INSERT INTO "public"."line_processes" ("id", "code", "created_at", "updated_at", "name", "edges", "style", "background") VALUES
(3, '4BC1YT', '2023-06-07 10:39:37+07', '2023-06-07 18:22:06+07', 'Line 1', '[]', NULL, '{"type":"color","background":"#3498db"}');

INSERT INTO "public"."machines" ("id", "name", "line_process_id", "created_at", "updated_at", "area_id", "port_in", "port_out", "size", "position", "style", "image_offline", "image_run", "image_stop", "status", "timeout", "last_call") VALUES
(11, 'Machine 1', 3, '2023-06-07 18:20:35+07', '2023-06-12 08:39:55+07', 6, 8000, 5000, '2', '{"machine":{"x":-80,"y":-60},"sensor":{"y":400,"x":200}}', NULL, 'uploads/machine-1686136834741-♡.png', 'uploads/machine-1686136834750-202209137522.png', 'uploads/machine-1686136834751-202302142177.png', 'OFFLINE', 1, '2023-06-12 08:39:50+07'),
(12, 'Machine 2', 3, '2023-06-07 19:21:23+07', '2023-06-12 08:39:55+07', 6, 1, 2, '2', '{"machine":{"x":100,"y":-60},"sensor":{"y":400,"x":400}}', NULL, 'uploads/machine-1686140482723-download-will-smith-face-image-5.png', 'uploads/machine-1686140482726-202302142177.png', 'uploads/machine-1686140482726-abbasyi.png', 'OFFLINE', NULL, '2023-06-12 08:39:50+07');

INSERT INTO "public"."tag_names" ("id", "name", "visibility", "created_at", "updated_at", "type", "description") VALUES
(16, '/iot/testing', 'Public', '2023-06-08 19:40:38+07', '2023-06-08 19:40:38+07', 'Number', 'Machine ');

INSERT INTO "public"."tag_names_machines" ("tag_name_id", "machine_id") VALUES
(16, 11);

INSERT INTO "public"."users" ("id", "name", "phone", "email", "password", "photo", "created_at", "updated_at", "type", "shift_id") VALUES
(14, 'Admin', '087', 'admin@gmail.com', '$2y$10$CEa/CBzDfGP4pUb/q4pTZuhROlVsoLkxrWha9s3W964Jn9dbtjxeK', 'asd.png', '2023-06-07 10:26:07+07', '2023-06-07 10:26:07+07', 'admin', 3);

ALTER TABLE "public"."alarms" ADD FOREIGN KEY ("tag_name_id") REFERENCES "public"."tag_names"("id");
ALTER TABLE "public"."machines" ADD FOREIGN KEY ("line_process_id") REFERENCES "public"."line_processes"("id");
ALTER TABLE "public"."machines" ADD FOREIGN KEY ("area_id") REFERENCES "public"."areas"("id");
ALTER TABLE "public"."tag_names_machines" ADD FOREIGN KEY ("tag_name_id") REFERENCES "public"."tag_names"("id");
ALTER TABLE "public"."tag_names_machines" ADD FOREIGN KEY ("machine_id") REFERENCES "public"."machines"("id");
