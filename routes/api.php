<?php

use Carbon\Carbon;
use App\Models\DataLog;
use App\Models\Setting;
use App\Models\LogImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/snap', function (Request $request) {
    $lastLog = LogImage::first();
    $currentDateTime = now();

    $lastLog->update([
        'operator' => $request->operator,
        'name' => '',
        'uuid' => '',
        'picture' => $request->SanpPic,
        'temperature' => $request->info['Temperature'],
        'time' => $currentDateTime,
        'mask' => $request->info['isNoMask'],
        'is_export' => '0'
    ]);
});

Route::post('/heartbeat', function (Request $request) {
    $lastLog = LogImage::orderBy('created_at', 'desc')->first();
    $currentDateTime = now();

    $lastLog->update([
        'heartbeat' => $currentDateTime,
    ]);
});
Route::post('/verify', function (Request $request) {
    $lastLog = LogImage::orderBy('created_at', 'desc')->first();
    $customizeID = $request->info['CustomizeID'];
    $currentDateTime = now();


    $lastLog->update([
        'operator' => $request->operator,
        'name' => $request->info['Name'],
        'uuid' => $customizeID,
        'picture' => $request->SanpPic,
        'time' => $currentDateTime,
        'temperature' => $request->info['Temperature'],
        'mask' => $request->info['isNoMask'],
        'is_export' => '0'
    ]);
});

Route::get('/lastlog', function () {
    $lastLog = App\Models\LogImage::orderBy('created_at', 'desc')->first();
    $infoText = App\Models\Setting::where('name', 'info')->first()->description;

    $message = str_replace(['$nama', '$suhu'], ["<b>$lastLog->name</b>", "<b>$lastLog->temperature</b>"], $infoText);

    return response()->json(['lastLog' => $lastLog, 'message' => $message]);
});


Route::post('/export', function (Request $request) {
    if ($request->response == 'Yes') {
        DataLog::create([
            'uuid' => $request->data['uuid'],
            'picture' => $request->data['picture'],
            'time' => $request->data['time'],
            'temperature' => $request->data['temperature'],
        ]);
    }
    LogImage::where('id', 1)->update([
        'is_export' => true
    ]);
    return response()->json(['status' => 'success']);
});

Route::get('/lastdatalog', function () {
    $log = App\Models\DataLog::oldest('time')->first();
    $device = App\Models\Setting::where('name', 'id_device')->first()->description;
    return response()->json(['device' => $device, 'log' => $log]);
});

Route::post('/statuslog', function (Request $request) {
    Setting::updateOrCreate(
        ['name' => 'status_log'],
        ['description' => $request->status]
    );
    return response()->json(['message' => 'Status log updated successfully']);
});

Route::get('/statuslog', function () {
    $statusLog = Setting::where('name', 'status_log')->first()->description;
    return $statusLog;
});

Route::post('/deletedatalog', function (Request $request) {
    if ($request->device != App\Models\Setting::where('name', 'id_device')->first()->description) {
        return response()->json(['status' => 'error', 'msg' => 'device salah']);
    }
    DataLog::where('id', $request->id_log)->delete();
    return response()->json(['status' => 'success', 'msg' => 'berhasil hapus data']);
});
