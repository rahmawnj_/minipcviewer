<?php

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::put('settings-update', function (Request $request) {
    $request->validate([
        'id_device' => 'required',
        'mask_text' => 'required',
        'confirm_text' => 'required',
        'stranger_text' => 'required',
        'info' => 'required',
    ]);

    // Simpan data ke database
    Setting::updateOrCreate(['name' => 'id_device'], ['description' => $request->id_device]);
    Setting::updateOrCreate(['name' => 'mask_text'], ['description' => $request->mask_text]);
    Setting::updateOrCreate(['name' => 'confirm_text'], ['description' => $request->confirm_text]);
    Setting::updateOrCreate(['name' => 'stranger_text'], ['description' => $request->stranger_text]);
    Setting::updateOrCreate(['name' => 'info'], ['description' => $request->info]);

    // Proses upload file MP3 jika ada
    if ($request->hasFile('error_sound')) {
        $file = $request->file('error_sound');
        $file->storeAs('public', 'error_sound.mp3'); // Simpan file dengan nama 'error_sound.mp3' di folder 'storage/app/public'

        // Simpan nama file ke database
        Setting::updateOrCreate(['name' => 'error_sound'], ['description' => 'error_sound.mp3']);
    }

    if ($request->hasFile('confirm_sound')) {
        $file = $request->file('confirm_sound');
        $file->storeAs('public', 'confirm_sound.mp3'); // Simpan file dengan nama 'confirm_sound.mp3' di folder 'storage/app/public'

        // Simpan nama file ke database
        Setting::updateOrCreate(['name' => 'confirm_sound'], ['description' => 'confirm_sound.mp3']);
    }


    return redirect()->back()->with('success', 'Settings updated successfully');
})->name('settings.update');
